| Zip Name | Unzipped Name | Date Uploaded to Google Drive |
| -------- | ------------- | ----------------------------- |
| ### MILESTONE 3's | | |
| M3 V1 | M3 V0.41 | 11-28-16 --- identical to 'M3 V0.41' (not committed) |
| ~ M3 | M2 V0.22 (1) | 11-26-16 --- w~ 11:29pm (re-submitted M2 to ilearn, not committed) ~ |
| M3 V0.41 | | 11-26-16 --- me 6:35pm |
| M3 V0.4 | | 11-26-16 --- me 3:30pm |
| M3 V0.31 | | 11-21-16 |
| ~ M2 V0.22 (1) | M2 V0.22 | 11-20-16 --- w~ (insequential, not committed) ~ |
| M3 V0.3 | | 11-18-16 |
| M3 V0.21 | | 11-16-16 |
| M3 V0.2 | | 11-15-16 |
| M3 V0.11 | | 11-15-16 |
| M3 V0.1 | | 11-15-16 |s
| ### MILESTONE 4's | | |
| M4 V0.41 | M4 V0.4 | --- w~, identical to my 0.4, but includes cloned copy further down and has random files (not committed) |
| ### FINALS | | |
| Final V1.15 | | --- clay, date is after 1.14 in progress (forked?) |