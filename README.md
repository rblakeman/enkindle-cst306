# Enkindle CST306

##### Final project from Fall 2016 CST-306 Game Development class.

###### Itch.io: [https://studentgames.itch.io/enkindle](https://studentgames.itch.io/enkindle)

> Unity Version 5.5.0f3

## Credits

Designer: [Emily Turnage](https://www.linkedin.com/in/emturnage/)

Developers: [Ryan Blakeman](https://ryanblakeman.com/), [Clay Evans](https://pcevans.github.io/)

Art: [Mateusz Cios](https://www.linkedin.com/in/mateusz-cios-5639b310a/), Magdalena Jabłonka, Wojciech Wilsz

Sounds: Tobiasz Itner

Additional Support: Brandon Woodard

