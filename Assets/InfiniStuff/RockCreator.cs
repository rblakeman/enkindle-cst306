﻿/*
name: Ryan Blakeman
course: CST306
*/
using UnityEngine;
using System.Collections.Generic;

public class RockCreator : MonoBehaviour
{
	public Terrain terrain;
	private TerrainData terraindata;
    public GameObject water;
	public float terrainSizeX;
	public float terrainSizeZ;
	public float heightOfWater;

	private Vector3[] rocks;
	public int rockcount;
	public List<GameObject> rockPrefabs;
	private bool good;

	// Use this for initialization
	void Start()
	{
        heightOfWater = water.transform.position.y;
		terraindata = terrain.terrainData;
		terrainSizeX = terraindata.size.x;
		terrainSizeZ = terraindata.size.z;

		good = false;
		rocks = new Vector3[rockcount];
		int randRock = Random.Range(0, rockPrefabs.Count - 1);
		Vector3 randPoint = new Vector3(0, 0, 0);
		List<GameObject> rock = new List<GameObject>(rockcount);
		GameObject rocksholder = new GameObject("Rocks");

        Debug.Log("Start Rocks");

        for (int i = 0; i < rockcount; i++)
		{
			good = false;
			while (!good)
			{
				randPoint = new Vector3(Random.Range(100, terrainSizeX), 0, Random.Range(10, terrainSizeZ));
				bool good2 = true;
				for (int j = 0; j < i; j++)
				{
					if (randPoint == rocks [j])
					{
                        if (randPoint.y < heightOfWater)
                        {
                            good2 = false;
                        }
					}
				}
				if (good2 == true)
					good = true;
			}
			rocks[i] = randPoint;

			randPoint.y = terrain.SampleHeight(randPoint);
            randRock = Random.Range(0, (rockPrefabs.Count));
            float randScale = Random.Range(1f, 2f); //(40f, 100f);
            float randRotationY = Random.Range(-180f, 180f);
            //Vector3 randRotation = new Vector3(Random.Range(0f, 359f), Random.Range(-180f, 180f), Random.Range(0f, 359f));

            if (randRock == 0) {
				Debug.Log ("ROCK 0: " + rockPrefabs[randRock].ToString());
                rockPrefabs[randRock].transform.localScale = new Vector3(randScale, randScale, randScale);
                rockPrefabs[randRock].transform.rotation = Quaternion.Euler(0, randRotationY, 0);
            }
			else if (randRock == 1) {
				Debug.Log ("ROCK 1: " + rockPrefabs[randRock].ToString());
                rockPrefabs[randRock].transform.localScale = new Vector3(randScale, randScale, randScale);
                rockPrefabs[randRock].transform.rotation = Quaternion.Euler(0, randRotationY, 0);
            }
			else if (randRock == 2) {
				Debug.Log ("ROCK 2: " + rockPrefabs[randRock].ToString());
                rockPrefabs[randRock].transform.localScale = new Vector3(randScale, randScale, randScale);
                rockPrefabs[randRock].transform.rotation = Quaternion.Euler(0, randRotationY, 0);
            }

            GameObject newrock = (GameObject)Instantiate(rockPrefabs[randRock], randPoint, rockPrefabs[randRock].transform.rotation, rocksholder.transform);
			newrock.name = "Rock" + (i + 1);
			rock.Add(newrock);
		}

        Debug.Log("Stop Rocks");
    }

	// Update is called once per frame
	void Update()
	{

	}
}
