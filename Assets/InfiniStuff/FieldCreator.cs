﻿/*
name: Ryan Blakeman
course: CST306
*/
using UnityEngine;
using System.Collections.Generic;

public class FieldCreator : MonoBehaviour
{
    public Terrain terrain;
    private TerrainData terraindata;
    public GameObject water;
    public float terrainSizeX;
    public float terrainSizeZ;
    public float heightOfWater;

    private Vector3[] grasses;
    public int grasscount;
    public List<GameObject> grassPrefabs;
    private bool good;

    // Use this for initialization
    void Start()
    {
        heightOfWater = water.transform.position.y;
        terraindata = terrain.terrainData;
        terrainSizeX = terraindata.size.x;
        terrainSizeZ = terraindata.size.z;

        good = false;
        grasses = new Vector3[grasscount];
        int randGrass = Random.Range(0, grassPrefabs.Count - 1);
        Vector3 randPoint = new Vector3(0, 0, 0);
        List<GameObject> grass = new List<GameObject>(grasscount);
        GameObject fieldholder = new GameObject("Field");

        Debug.Log("Start Grass");

        for (int i = 0; i < grasscount; i++)
        {
            good = false;
            while (!good)
            {
                randPoint = new Vector3(Random.Range(0, terrainSizeX), 0, Random.Range(0, terrainSizeZ));
                bool good2 = true;
                for (int j = 0; j < i; j++)
                {
                    if (randPoint == grasses[j])
                    {
                        if (randPoint.y < heightOfWater)
                        {
                            good2 = false;
                        }
                    }
                }
                if (good2 == true)
                    good = true;
            }
            grasses[i] = randPoint;

            randPoint.y = terrain.SampleHeight(randPoint);
            randGrass = Random.Range(0, (grassPrefabs.Count));
            //grassPrefabs[randGrass].transform.localScale = new Vector3(1.25f, 0.75f, 1.25f);

            //float randRotationY = Random.Range(-180f, 180f);

            //Vector3 angles = grassPrefabs[randGrass].transform.rotation.eulerAngles;
            if (randGrass == 0)
            {
                Debug.Log("GRASS 0: " + grassPrefabs[randGrass].ToString());
                //grassPrefabs[randGrass].transform.localScale = new Vector3(1.25f, 0.75f, 1.25f);
                //grassPrefabs[randGrass].transform.rotation = Quaternion.Euler(angles.x, randRotationY, angles.z);
            }
            else if (randGrass == 1)
            {
                Debug.Log("GRASS 1: " + grassPrefabs[randGrass].ToString());
                //grassPrefabs[randGrass].transform.localScale = new Vector3(1.25f, 0.75f, 1.25f);
                //grassPrefabs[randGrass].transform.rotation = Quaternion.Euler(angles.x, randRotationY, angles.z);
            }
           /* else if (randGrass == 2)
            {
                Debug.Log("GRASS 2: " + grassPrefabs[randGrass].ToString());
                grassPrefabs[randGrass].transform.localScale = new Vector3(1.25f, 0.75f, 1.25f);
                //grassPrefabs[randGrass].transform.rotation = Quaternion.Euler(angles.x, randRotationY, angles.z);
            }*/

            GameObject newgrass = (GameObject)Instantiate(grassPrefabs[randGrass], randPoint, grassPrefabs[randGrass].transform.rotation, fieldholder.transform);
            newgrass.name = "Grass" + (i + 1);
            grass.Add(newgrass);
        }
        Debug.Log("Stop Grass");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
