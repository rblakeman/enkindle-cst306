﻿/*
name: Philip Evans
course: CST306
*/
using UnityEngine;
using System.Collections.Generic;

public class ForestCreator : MonoBehaviour
{
    public int numberOfClusters;
    public float spaceBetweenClusters;
    public float heightOfWater;
    public float[] spaceBetweenTrees;
    public int[] numberOfTreesInClusters;
    public List<GameObject> treePrefabs;
    public Terrain terrain;
    private List<GameObject>[] forests;
    private Vector3[] centers;
    public int realCount;
    public bool done;

    public static int MAXFORESTDIST = 100;

    void Start()
    {
        done = false;
        realCount = 0;
        forests = new List<GameObject>[numberOfClusters];
        centers = new Vector3[numberOfClusters];

        float terrainSizeX = terrain.terrainData.size.x;
        float terrainSizeZ = terrain.terrainData.size.z;

        Debug.Log("Start Tree");

        for (int i = 0; i < numberOfClusters; i++)
        {
            GameObject forestholder = new GameObject("Forest " + (i + 1));
            List<GameObject> cluster = new List<GameObject>(numberOfTreesInClusters[i]);
            Vector3 center = new Vector3(0, 0, 0);
            int maxDistFromCenter = 10;

            for (int j = 0; j < numberOfTreesInClusters[i]; j++)
            {
                bool madeForest = false;
                int randTree = Random.Range(0, treePrefabs.Count - 1);
                Vector3 randPoint = new Vector3(0, 0, 0);
                bool madeTree = false;
                int treeAttempts = 0;
                int pointAttempts = 0;
                while (!madeTree)
                {
                    madeTree = true;

                    bool gotPoint = false;
                    pointAttempts = 0;
                    if (j == 0)
                    {
                        while (!gotPoint)
                        {
                            gotPoint = true;
                            randPoint = new Vector3(Random.Range(0, terrainSizeX), 0, Random.Range(0, terrainSizeZ));
                            randPoint.y = terrain.SampleHeight(randPoint);
                            if (i != 0)
                            {
                                for (int k = 0; k < i; k++)
                                {
                                    if (Vector3.Magnitude(centers[k] - randPoint) < spaceBetweenClusters || randPoint.y < heightOfWater)
                                    {
                                        gotPoint = false;
                                        pointAttempts++;
                                        break;
                                    }
                                }
                            }
                            if (gotPoint)
                            {
                                center = randPoint;
                                centers[i] = randPoint;
                                madeForest = true;
                            }
                            if (pointAttempts > 20)
                            {
                                madeTree = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        madeForest = true;
                        Vector3 randomizer = new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, 20));
                        randPoint = center + new Vector3(Random.Range(-maxDistFromCenter, maxDistFromCenter) + randomizer.x, 0, Random.Range(-maxDistFromCenter, maxDistFromCenter) + randomizer.z);
                        randPoint.y = terrain.SampleHeight(randPoint);
                        foreach (GameObject c in cluster)
                        {
                            if (Vector3.Magnitude(c.transform.position - randPoint) < spaceBetweenTrees[i] || (randPoint.x < 0 || randPoint.x > terrainSizeX) || (randPoint.z < 0 || randPoint.z > terrainSizeZ) || randPoint.y < heightOfWater)
                            {
                                treeAttempts++;
                                madeTree = false;
                                break;
                            }
                        }
                    }

                    if (treeAttempts > 5 && maxDistFromCenter < MAXFORESTDIST)
                    {
                        treeAttempts = 0;
                        maxDistFromCenter++;
                    }
                    else if (treeAttempts > 20 || pointAttempts > 20)
                        break;

                    if (madeTree)
                    {
                        cluster.Add(Instantiate(treePrefabs[randTree], randPoint, treePrefabs[randTree].transform.rotation, forestholder.transform) as GameObject);
                        realCount++;
                    }
                }
                if (!madeForest)
                    break;
            }
            if (cluster.Count > 0)
                forests[i] = cluster;
        }

        done = true;
        Debug.Log("Stop Tree");
    }
    void Update()
    {

    }
}
