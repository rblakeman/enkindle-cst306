﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//name: Brandon Woodard
public class sounds : MonoBehaviour
{
    public AudioSource audio;
    public AudioClip birdFlap;
    public AudioClip birdFlapUp;
    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.volume = 0.7f;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void soundInAir()
    {
        audio.PlayOneShot(birdFlap, 1);
    }
    void soundInAirUp()
    {
        audio.PlayOneShot(birdFlapUp, 0.5f);
    }

}
