﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerScript : MonoBehaviour {

    float elapsed;

	// Use this for initialization
	void Start () {
        elapsed = 0;
	}
	
	// Update is called once per frame
	void Update () {
        elapsed += Time.deltaTime;
        if (elapsed > .02f)
        {
            this.GetComponent<Light>().intensity = 4f + (1.25f * Random.Range(-1f, 1f));
            elapsed = 0;
        }
	}
}
