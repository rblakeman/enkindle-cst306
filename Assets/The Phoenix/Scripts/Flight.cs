﻿/*
name: Philip Evans, Ryan Blakeman
course: CST306
*/

using UnityEngine;
using System.Collections;

public class Flight : MonoBehaviour
{
    private Animator anim;
    private Transform tempTransform;
    private float constantspeed;
    private float speed;
    private float resetX;
	private int count;
    private float elapsed;

	public Terrain tr;
    public float waterHeight;

    void Start()
    {
      
        float randX = Random.Range(150f, 750f);
        float randZ = Random.Range(150f, 700f);
        float randY = tr.SampleHeight(new Vector3(randX, this.transform.position.y, randZ)) + 20f;
        this.transform.position = new Vector3(randX, randY, randZ);
	
        constantspeed = 0.25f;
        speed = 50;
        resetX = 10;

		tempTransform = new GameObject().transform;
		tempTransform.position = this.transform.position;
		tempTransform.rotation = this.transform.rotation;

        anim = GetComponentInChildren<Animator>();
        tempTransform.position = this.transform.position;
        tempTransform.rotation = this.transform.rotation;

        elapsed = 0;
    }

    void Update()
    {

        //Debug.Log("X: " + Input.GetAxis("Controller X (left)") + "\n" + "Y: " + Input.GetAxis("Controller Y (left)"));
		
        if (Time.timeScale != 0)
        {
            tempTransform.Translate(constantspeed, 0, 0);

            if (Input.GetKey(KeyCode.A) || Input.GetAxis("Mouse X") < 0 || Input.GetAxis("Controller X (left)") < 0) //bird movement is now mouse dependent for natural movement
            {
                tempTransform.Rotate(-Vector3.up * speed * Time.deltaTime);
                tempTransform.Rotate(Vector3.right * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D) || Input.GetAxis("Mouse X") > 0 || Input.GetAxis("Controller X (left)") > 0)
            {
                tempTransform.Rotate(Vector3.up * speed * Time.deltaTime);
                tempTransform.Rotate(Vector3.left * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.W) || Input.GetAxis("Mouse Y") > 0 || Input.GetAxis("Controller Y (left)") > 0)
            {
                tempTransform.Rotate(-Vector3.back * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S) || Input.GetAxis("Mouse Y") < 0 || Input.GetAxis("Controller Y (left)") < 0)
            {
				tempTransform.Rotate(Vector3.back * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.E) || Input.GetKey(KeyCode.JoystickButton5))
				tempTransform.Rotate(-Vector3.right * speed * Time.deltaTime);

            if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.JoystickButton4))
				tempTransform.Rotate(Vector3.right * speed * Time.deltaTime);

            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.JoystickButton0))
            {
                constantspeed = .5f;
                anim.SetBool("Fast", true);
            }
            else
            {
                constantspeed = 0.25f;
                anim.SetBool("Fast", false);
            }
            
			if (Input.GetKey (KeyCode.R) || Input.GetKey(KeyCode.JoystickButton2)) 
			{
				Quaternion target = Quaternion.Euler (0, tempTransform.eulerAngles.y, tempTransform.eulerAngles.z);
				if (Vector3.Dot (tempTransform.up, Vector3.down) > 0) { 
					target = Quaternion.Euler (180f, tempTransform.eulerAngles.y, tempTransform.eulerAngles.z);
				}

				if (tempTransform.rotation.x != 0) 
				{
					tempTransform.rotation = Quaternion.Slerp (tempTransform.rotation, target, Time.deltaTime * resetX);
				}
			}

			CorrectFlight ();

            this.transform.position = Vector3.Lerp(this.transform.position, tempTransform.position, .8f);
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, tempTransform.rotation, Time.deltaTime * 10);
        }
    }

	void CorrectFlight() {
		float goodHeight = tr.SampleHeight (tempTransform.position) + 2f;
		if (tempTransform.position.y < goodHeight) {
			tempTransform.position = new Vector3 (tempTransform.position.x, Mathf.Lerp (tempTransform.position.y, goodHeight, .2f), tempTransform.position.z);
		}
        if(tempTransform.position.y < waterHeight+1)
        {
            tempTransform.position = new Vector3(tempTransform.position.x, Mathf.Lerp(tempTransform.position.y, waterHeight+1, .2f), tempTransform.position.z);
        }

		Vector3 right = Vector3.Normalize (Vector3.Cross (Vector3.up, tempTransform.forward));
        if (Input.GetAxis("Mouse Y") == 0 && Input.GetAxis("Mouse X") == 0)
        {
            elapsed += Time.deltaTime;
            if (elapsed > .1f)
            {
                if (Vector3.Dot(tempTransform.up, right) != 0)
                {
                    Quaternion target = Quaternion.Euler(0, tempTransform.eulerAngles.y, tempTransform.eulerAngles.z);
                    if (Vector3.Dot(tempTransform.up, Vector3.down) > 0)
                    {
                        target = Quaternion.Euler(180f, tempTransform.eulerAngles.y, tempTransform.eulerAngles.z);
                    }

                    if (tempTransform.rotation.x != 0)
                    {
                        count += 1;
                        tempTransform.rotation = Quaternion.Slerp(tempTransform.rotation, target, Time.deltaTime);
                    }
                }
                else
                {
                    elapsed = 0;
                }
            }
        }
        else elapsed = 0;
	}
}