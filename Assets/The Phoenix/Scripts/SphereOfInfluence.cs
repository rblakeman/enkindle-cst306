﻿/*
name: Ryan Blakeman, Philip Evans
course: CST306
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SphereOfInfluence : MonoBehaviour
{
    public Camera fpsCam;
    public GameObject camera;
    public GameObject loadingmenu;
    public GameObject startmenu;
    private int nextUpdate;
    public bool finished;
    public Slider timer;
    public bool reset;

    public Terrain snow;
    private TerrainData snowData;
    public Terrain terrain;
    private TerrainData terrainData;
    float[,,] splatmapData;
    float[,,] snowmapData;
    private int heightmapWidth;
    private int heightmapHeight;
    public Vector2 p_pos; // phoenix pos
    public Vector2 t_pos; // terrain pos
    private int maxradius;
    public SphereCollider soi;

    public int percentage;
    //private List<Vector2> totalcoverage;
    private int totalcoverage;
    public List<Vector2> coverage;

    public Material[] materials;
    private int winter; // 0
    private int spring; // 1
    public List<GameObject> trees;
    private bool madeTrees;
    //private int tree_count;

    // bool array (for each tree)
    public bool[] booleans;

    // leaf array (m_r of leaves from tree[])
    public Renderer[] leaf_rend;

    public Material[] rock_materials;
    // rock array (m_r of each rock)
    public Renderer[] rock_rend;

    public List<Renderer> meltingRocks;
    public List<Renderer> growingLeaves;
    public List<GameObject> glowingLeaves;
    public List<Renderer> changingLeaves;

    public Vector3 starts1;
    public Vector3 ends1;

    //terrain dependant audio
    public AudioClip flapClip;
    public AudioSource startmenuaudio;
    static bool gameStarted;

    public ForestCreator fcs;



    void Start()
    {
        camera.SetActive(false); //fpsCam.enabled = false;
        loadingmenu.SetActive(true);
        Debug.Log("wait");
        finished = false;
        reset = false;

        growingLeaves = new List<Renderer>();
        changingLeaves = new List<Renderer>();

        //UnityEngine.Profiling.Profiler.maxNumberOfSamplesPerFrame = -1; // fixes profiler error
        nextUpdate = 1;

        terrainData = terrain.terrainData;
        snowData = snow.terrainData;
        heightmapWidth = terrainData.heightmapWidth;
        heightmapHeight = terrainData.heightmapHeight;
        maxradius = 20;
        p_pos.x = this.gameObject.transform.position.x;
        p_pos.y = this.gameObject.transform.position.z;
        t_pos.x = ((p_pos.x / terrainData.size.x) * heightmapWidth);
        t_pos.y = ((p_pos.y / terrainData.size.z) * heightmapHeight);

        totalcoverage = (int)terrainData.size.x * (int)terrainData.size.y;
        // generate list of all points based on terrain size
        /*for (int a = 0; a < terrainData.size.x; a++)
        {
            for (int b = 0; b < terrainData.size.y; b++)
            {
                totalcoverage.Add(new Vector2(a, b)); // should actually just use int size of tdata.x*tdata.z
            }
        }*/

        float[,] heights = snowData.GetHeights(0, 0, heightmapWidth, heightmapHeight);
        float[,] refHeights = terrainData.GetHeights(0, 0, heightmapWidth, heightmapHeight);
        for (int i = 0; i < heightmapWidth; i++)
        {
            for (int j = 0; j < heightmapWidth; j++)
            {
                heights[i, j] = refHeights[i, j] + .004f;
            }
        }
        snowData.SetHeights(0, 0, heights);

        winter = 0;
        spring = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (finished)
        {
            if (!reset)
            {
                loadingmenu.SetActive(false);
                startmenu.SetActive(true);
                camera.SetActive(true);
                fpsCam.enabled = true;
                startmenuaudio.enabled = true;
                Time.timeScale = 0;
                timer.value = 100;
                reset = true;
            }
        }

        // If the next update is reached
        if (Time.time >= nextUpdate)
        {
            //Debug.Log(Time.time + ">=" + nextUpdate);
            // Change the next update (current second+1)
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            // Call your function
            UpdateEverySecond();
        }

        if (!madeTrees)
        {
            makeForest();
        }


        float widthconversion = (float)heightmapWidth / terrainData.size.x;
        float heightconversion = (float)heightmapHeight / terrainData.size.y;
        //Vector3 forward = 30f * Vector3.Normalize(Vector3.Cross(fpsCam.transform.right, Vector3.up));
        p_pos.x = transform.position.x;// + forward.x;
        p_pos.y = transform.position.z;// + forward.z;
        t_pos.x = p_pos.x * widthconversion; // converts real world x to terrain x
        t_pos.y = p_pos.y * widthconversion; // converts real world z to terrain y

        Vector3 withoutY = new Vector3(p_pos.x, 0, p_pos.y);
        float height = snow.SampleHeight(withoutY) + snow.GetPosition().y;
        int radius = (int)soi.radius*3; //depends on the scale of the object
        bool proximity = (soi.transform.position.y <= height + radius);

        if (proximity && Time.timeScale != 0)
        {
            int realRadius = (int)(Mathf.Sqrt((Mathf.Pow(radius * widthconversion, 2) - Mathf.Pow((soi.transform.position.y - height)*widthconversion, 2))));

            if (realRadius != 0)
            {
                int startX = (int)t_pos.x - realRadius;
                int offsetX = 0;
                int offsetXdir = 0;
                int startY = (int)t_pos.y - realRadius;
                int offsetY = 0;
                int offsetYdir = 0;
                if (!(startX >= heightmapWidth || startY >= heightmapWidth || startX + 2 * realRadius < 0))
                {
                    if (startX < 0)
                    {
                        offsetX = 0 - startX;
                        offsetXdir = -1;
                        startX = 0;
                    }
                    else if (startX + 2 * realRadius >= heightmapWidth)
                    {
                        offsetX = (startX + 2 * realRadius) - (heightmapWidth - 1);
                        offsetXdir = 1;
                        //startX = heightmapWidth - 1;
                    }

                    if (startY < 0)
                    {
                        offsetY = 0 - startY;
                        offsetYdir = -1;
                        startY = 0;
                    }
                    if (startY + 2 * realRadius >= heightmapWidth)
                    {
                        offsetY = (startY + 2 * realRadius) - (heightmapWidth - 1);
                        offsetYdir = 1;
                        //startY = heightmapWidth - 1;
                    }
                    int diameterX = realRadius * 2 - offsetX;
                    int diameterY = realRadius * 2 - offsetY;

                    float[,] heights = snowData.GetHeights(startX, startY, diameterX, diameterY); // good up to this point

                    for (int yy = -realRadius; yy <= realRadius; yy++)
                    {
                        for (int xx = -realRadius; xx <= realRadius; xx++)
                        {
                            float d = (xx * xx + yy * yy) / (realRadius * realRadius);

                            if (d < 1.0) // if point is in the circle
                            {
                                int xxmap = xx + realRadius + (offsetX * offsetXdir); //maps from circle @ origin to heightmap
                                int yymap = yy + realRadius + (offsetY * offsetYdir);
                                if (xxmap >= 0 && xxmap < diameterX && yymap >= 0 && yymap < diameterY)
                                    heights[yymap, xxmap] -= heightconversion * .00008f * (1f - d);
                            }
                        }
                    }

                    snowData.SetHeights(startX, startY, heights);
                }
            }
            
        }

        for (int i = 0; i < growingLeaves.Count; i++)
        {
            float alpha = growingLeaves[i].material.GetFloat("_Alpha");
            growingLeaves[i].material.SetFloat("_Alpha", Mathf.Lerp(alpha,1f,.05f));
            if (alpha >= .99f)
            {
                growingLeaves.RemoveAt(i);
                GameObject g = glowingLeaves[i];
                Transform t = g.transform.FindChild("TreeHolder");
                t.Translate(new Vector3(0, 10, 0));
                Light l = t.gameObject.AddComponent<Light>();
                l.intensity = 1f;
                l.range = 20f;
                glowingLeaves.RemoveAt(i);
            }
        }

        for (int i = 0; i < changingLeaves.Count; i++)
        {
            changingLeaves[i].material.Lerp(changingLeaves[i].material, materials[spring], .05f);
            if (changingLeaves[i].material.color == materials[spring].color)
            {
                changingLeaves.RemoveAt(i);
            }
            
        }

        for (int i = 0; i < meltingRocks.Count; i++)
        {
            float blend = meltingRocks[i].material.GetFloat("_SnowGeneralblending");
            meltingRocks[i].material.SetFloat("_SnowGeneralblending", Mathf.Lerp(blend, 1, .05f));

        }
    }

    void UpdateEverySecond()
    {
        //percentage = (int)(((float)coverage.Count / totalcoverage) * 100);
        //Debug.Log("percentage: " + percentage);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Leaf")
        {
            Renderer r = other.GetComponent<Renderer>();
            if (r.material.GetFloat("_Alpha") == 0)
            {
                growingLeaves.Add(r);
                glowingLeaves.Add(other.transform.parent.gameObject);
                r.enabled = true;
            }
            else
                changingLeaves.Add(r);
        }

        if (other.gameObject.tag == "Rock")
        {
            Renderer r = other.GetComponent<Renderer>();
            if (r.material.GetFloat("_SnowGeneralblending") == 0)
            {
                meltingRocks.Add(r);
            }
        }
    }

    void makeForest()
    {
        trees = new List<GameObject>(GameObject.FindGameObjectsWithTag("INfiniDyForest"));
        if(fcs.done && trees.Count == fcs.realCount) {
            madeTrees = true;
            for (int i = 0; i < trees.Count; i++)
            {
                trees[i].transform.localScale = new Vector3(2, 2, 2);
            }
            leaf_rend = new Renderer[trees.Count];
            StartCoroutine(CollectLeaves());
        }
    }

    IEnumerator CollectLeaves()
    {
        Debug.Log("Start Leaves");
        Time.timeScale = 1;
        yield return new WaitForSecondsRealtime(1);
        Time.timeScale = 0;
        int leaves = 0;
        for (int i = 0; i < trees.Count; i++)
        {
            GameObject leaf = null;
            if (trees[i].transform.Find("INfiniTREE LEAF Individual Red" + i) != null)
                leaf = trees[i].transform.Find("INfiniTREE LEAF Individual Red" + i).gameObject;
            else if (trees[i].transform.Find("INfiniTREE LEAF CHERRY" + i) != null)
                leaf = trees[i].transform.Find("INfiniTREE LEAF CHERRY" + i).gameObject;
            if (leaf != null)
            {
                leaves++;
                MeshCollider meshc = leaf.AddComponent(typeof(MeshCollider)) as MeshCollider;
                leaf_rend[i] = leaf.GetComponent<Renderer>();
                meshc.sharedMesh = leaf.GetComponent<MeshFilter>().mesh;
                leaf_rend[i].material = materials[winter];
                leaf_rend[i].enabled = false;
                leaf_rend[i].material.SetFloat("_Alpha",0f);
            }
        }

        if (leaves == trees.Count)
        {
            finished = true;
            Debug.Log("Stop Leaves");
            Debug.Log("finished");
        }
        else
            StartCoroutine(CollectLeaves());
    }
}