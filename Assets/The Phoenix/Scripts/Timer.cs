﻿/*
name: Ryan Blakeman, Philip Evans
course: CST306
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Timer : MonoBehaviour {

    public Slider timer;
    public int max;
    public bool started = false;
    public float seconds, mins, previous_seconds;

	public GameObject Phoenix;
	public Camera cam;
	public Camera flyover1;
	public Camera flyover2;
    public GameObject finalmusic;
    public GameObject credits;

    public Light sun;
    public ParticleSystem snowing;
    //public BloomOptimized bloom;

    // Use this for initialization
    void Start () {
        max = 100;
        timer.value = max;

        mins = (int)(Time.time / 60f);
        seconds = (int)(Time.time % 60f);
        previous_seconds = seconds;

        finalmusic.SetActive(false);
        credits.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Timer: " + timer.value);

        mins = (int)(Time.time / 60f);
        seconds = (int)(Time.time % 60f);

        sun.intensity = .4f + (.35f * ((100f - timer.value)/100));

        if (seconds != previous_seconds)
        {
            timer.value = timer.value - .25f;
        }

        if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.JoystickButton0)) && Time.timeScale > 0)
        {
            timer.value = timer.value - .01f;
        }

        if (timer.value < timer.maxValue &&
            timer.value > timer.maxValue / 2)
        {
            snowing.maxParticles = 5000;
        }

        else if (timer.value < timer.maxValue / 2 &&
                timer.value > timer.maxValue / 3)
        {
            snowing.maxParticles = snowing.maxParticles / 2;
        }

        else if (timer.value < timer.maxValue / 3 &&
                timer.value > timer.maxValue / 4)
        {
            snowing.maxParticles = snowing.maxParticles / 2;
        }

        else if (timer.value < timer.maxValue / 4)
        {
            snowing.maxParticles = snowing.maxParticles / 2;
        }

        //if (timer.value < timer.maxValue / 20)
        //{
           // bloom.intensity = Mathf.Lerp(bloom.intensity, 10f, 0.2f);
        //}

        if (timer.value == 0 && !started)
        {
            snowing.maxParticles = 0;
			StartCoroutine(Flyover1 ());
            //Application.Quit();
            started = true;
        }

        previous_seconds = seconds;
    }

	IEnumerator Flyover1()
    {
		cam.gameObject.SetActive (false);
		flyover1.gameObject.SetActive (true);
        finalmusic.SetActive(true);
		Phoenix.SetActive (false);
        yield return new WaitForSeconds(12);
		StartCoroutine(Flyover2 ());
    }

	IEnumerator Flyover2()
	{
		flyover1.gameObject.SetActive (false);
		flyover2.gameObject.SetActive (true);
		yield return new WaitForSeconds(24);
		StartCoroutine (Quit ());
	}

	IEnumerator Quit()
	{
        credits.SetActive(true);
        yield return new WaitForSeconds(7);
		Application.Quit();
	}
}
