﻿using UnityEngine;
using System.Collections;

public class FollowPhoenix : MonoBehaviour {

	public Transform target;
	public Transform anchor;
    public Quaternion tmpRot;
    public Transform tmpTrans;
	// Use this for initialization
	void Start () {
        tmpTrans = new GameObject().transform;
        transform.LookAt(tmpTrans);
    }
	
	// Update is called once per frame
	void Update () {
   
        tmpTrans.LookAt(target); //Quaternion.Lerp(tmpRot,target.rotation,Time.deltaTime * 10);
        tmpTrans.position = anchor.position;

		transform.position = tmpTrans.position;
        transform.rotation = Quaternion.Slerp(transform.rotation, tmpTrans.rotation, Time.deltaTime * 10);

        //Debug.Log(target.rotation);

	}
}
