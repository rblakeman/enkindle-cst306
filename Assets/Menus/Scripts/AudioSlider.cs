﻿/*
name: Brandon Woodard
course: CST306
*/
using UnityEngine;
using System.Collections;

public class AudioSlider : MonoBehaviour {

    public void changeVolume(float newVolume)
    {
        PlayerPrefs.SetFloat("volume", newVolume);
        AudioListener.volume = PlayerPrefs.GetFloat("volume");
    }


}
