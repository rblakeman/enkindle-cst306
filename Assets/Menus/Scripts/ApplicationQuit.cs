﻿/*
name: Brandon Woodard
course: CST306
*/
using UnityEngine;
using System.Collections;

public class ApplicationQuit : MonoBehaviour {
  
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
