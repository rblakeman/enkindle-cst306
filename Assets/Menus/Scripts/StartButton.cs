﻿/*
name: Ryan Blakeman, Philip Evans, Brandon Woodard
course: CST306
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour {

    public Button startbutton;
    //public AsyncOperation capture;
    //public float progress;

    public GameObject mainmenu;
    public GameObject loadingmenu;

    void Start()
    {
        startbutton = this.GetComponent<Button>();
        startbutton.onClick.AddListener(OnClick);

        //capture = SceneManager.LoadSceneAsync("Enkindle Landscape", LoadSceneMode.Single);
        //capture.allowSceneActivation = false;
    }
    
    void Update()
    {
        //progress = capture.progress;
        //Debug.Log(progress);
    }

    void OnClick()
    {
        mainmenu.SetActive(false);
        loadingmenu.SetActive(true);
        //capture.allowSceneActivation = true;
        SceneManager.LoadScene("Enkindle Landscape");
    }

}
