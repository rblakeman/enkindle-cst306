﻿/*
name: Brandon Woodard
course: CST306
*/
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class QuitButton : MonoBehaviour {
    public Button qBtn;
    void Start()
    {
        qBtn.onClick.AddListener(TaskOnClick);
    }
    void TaskOnClick()
    {
        Application.Quit();
        Debug.Log("You have clicked button");
    }
	
}
