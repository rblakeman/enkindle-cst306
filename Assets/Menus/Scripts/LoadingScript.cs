﻿/*
name: Ryan Blakeman
course: CST306
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour
{
    public GameObject loadingmenu;

    // Use this for initialization
    void Start()
    {
        loadingmenu.SetActive(true);
        StartCoroutine(LoadEnkindle());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator LoadEnkindle()
    {
        Debug.Log("Start Loading");
        yield return new WaitForSecondsRealtime(1);
        SceneManager.LoadScene("Enkindle Landscape");
    }
}