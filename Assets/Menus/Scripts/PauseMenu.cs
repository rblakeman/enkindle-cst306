﻿/*
name: Ryan Blakeman
course: CST306
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseMenu : MonoBehaviour {

    CursorLockMode wantedMode;
    public GameObject loadingmenu;
    public AudioSource ingamemusic;

    public GameObject startmenu;
    public Button startbutton;
    public bool started;
    public GameObject pausemenu;
    public Button resumebutton;
    public bool paused;

	// Use this for initialization
	void Start ()
    {
        loadingmenu.SetActive(true);
        Cursor.lockState = wantedMode = CursorLockMode.None;
        SetCursorState();

        startmenu.gameObject.SetActive(false);
        startbutton.onClick.AddListener(onClickStart);
        started = false;

        pausemenu.gameObject.SetActive(false);
        resumebutton.onClick.AddListener(OnClickPause);
        paused = false;

        ingamemusic.enabled = false;
	}

    // Update is called once per frame
    void Update()
    {

        // cursor lock
        if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton7)) && started == true)
        {
            paused = !(paused);
            Cursor.lockState = wantedMode = CursorLockMode.None;
            SetCursorState();
        }

        if (Input.GetKeyDown(KeyCode.Escape) && started == false)
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }

        if (paused && started == true)
        {
            Debug.Log("paused");
            Time.timeScale = 0;
            pausemenu.gameObject.SetActive(true);
        }
        else if (!paused && started == true)
        {
            wantedMode = CursorLockMode.Locked;
            SetCursorState();

            Debug.Log("!paused");
            Time.timeScale = 1;
            pausemenu.gameObject.SetActive(false);
        }
    }

    void onClickStart()
    {
        startmenu.SetActive(false);
        started = true;
        Time.timeScale = 1;
        ingamemusic.enabled = true;
    }

    void OnClickPause()
    {
        paused = false;
    }

    // Apply requested cursor state
    void SetCursorState()
    {
        Cursor.lockState = wantedMode;
        // Hide cursor when locking
        Cursor.visible = (CursorLockMode.Locked != wantedMode);
    }
}
